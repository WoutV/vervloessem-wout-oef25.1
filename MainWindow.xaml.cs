﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vervloessem_Wout_oef25._1;

namespace Vervloessem_Wout_oef25._1v2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      
        string sKanaal = "0";
        string sVolume = "0";
        int iWelkeTv = 0;
        TV teve1 = new TV();
        TV teve2 = new TV();
        public MainWindow()
        {

            InitializeComponent();
            tbKanaal.Text = "0";
            tbVolume.Text = "0";
            tbKanaal.Focusable = false;
            tbVolume.Focusable = false;

        }

      

   

        private void rbSamsung_Checked(object sender, RoutedEventArgs e)
        {
            iWelkeTv = 1;
         
            teve1.Merk = "Samsung";
            teve1.Hertz = 100;
            teve1.Type = " QLED 8K 85Q950TS";
            teve1.Beeldgrootte = 1300;
            tbKanaal.Text = teve1.Kanaal.ToString();
            tbVolume.Text = teve1.Volume.ToString();
            lblSpecs.Content = teve1.ToString();
            imgTV.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"/Fotos/Samsung QLED 8K 85Q950TS (2020).jpg"));
        }

        private void rbSony_Checked(object sender, RoutedEventArgs e)
        {
            iWelkeTv = 2;
           
            teve2.Merk = "Sony";
            teve2.Hertz = 60;
            teve2.Type = " KD-77AG9";
            teve2.Beeldgrootte = 1300;
            tbKanaal.Text = teve2.Kanaal.ToString();
            tbVolume.Text = teve2.Volume.ToString();
            lblSpecs.Content = teve2.ToString();

        
           
            imgTV.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"/Fotos/Sony KD-77AG9.jpg"));
            
       
        }
        private void rbSony_Unchecked(object sender, RoutedEventArgs e)
        {
            teve2.Kanaal = Convert.ToInt32(tbKanaal.Text);
            teve2.Volume = Convert.ToInt32(tbVolume.Text);

        }
        private void rbSamsung_Unchecked(object sender, RoutedEventArgs e)
        {
            teve1.Kanaal = Convert.ToInt32(tbKanaal.Text);
            teve1.Volume = Convert.ToInt32(tbVolume.Text);

        }
        private void cbPower_Checked(object sender, RoutedEventArgs e)
        {
            tbKanaal.Focusable = true;
            tbVolume.Focusable = true;
            if (iWelkeTv==1)
            {
                tbKanaal.Text = teve1.Kanaal.ToString();
                tbVolume.Text = teve1.Volume.ToString();

            }
            if (iWelkeTv == 2)
            {
                tbKanaal.Text = teve2.Kanaal.ToString();
                tbVolume.Text = teve2.Volume.ToString();

            }

        }
        private void cbPower_Unchecked(object sender, RoutedEventArgs e)
        {
            tbKanaal.Focusable = false;
            tbVolume.Focusable = false;

        }

        private void tbKanaal_TextChanged(object sender, TextChangedEventArgs e)
        {
            sKanaal = tbKanaal.Text;
           
        }

        private void tbVolume_TextChanged(object sender, TextChangedEventArgs e)
        {
            sVolume = tbVolume.Text;
        }
    }
}
