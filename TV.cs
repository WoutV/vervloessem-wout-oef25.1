﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vervloessem_Wout_oef25._1
{
    class TV
    {
        //private string _afbeelding;
        //private int _beeldgrootte;
        //private int _hertz;
        //private int _kanaal;
        //private string _merk;
        //private bool _power;
        //private bool _teletekst;
        //private string _type;
        //private int _volume;

        public string Afbeelding { get; set; }
        public int Beeldgrootte { get; set; }
        public int Hertz { get; set; }
        public int Kanaal { get; set; }
        public string Merk { get; set; }
        public bool Power { get; set; }
        public bool Teletekst { get; set; }

        public string Type { get; set; }
        public int Volume { get; set; }

        public TV()
        {
            this.Volume = 0;
            this.Kanaal = 0;
        }
        public TV(string merk, string type, int hertz, int beeldgrootte, string afbeelding)
        {
            this.Volume = 0;
            this.Kanaal = 0;
            Merk = merk;
            Type = type;
            Hertz = hertz;
            Beeldgrootte = beeldgrootte;
            Afbeelding = afbeelding;
        }
        public override string ToString()
        {
            return "Merk:" + Merk + Environment.NewLine +
                    "Type: " + Type + Environment.NewLine +
                    "Herz: " + Hertz + Environment.NewLine +
                    "Beeldgrootte: " + Beeldgrootte;
                

        }

    }
}
